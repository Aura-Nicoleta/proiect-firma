#include "Traseu.h"

CTraseu::CTraseu()
{
}

CTraseu::CTraseu(COras start, COras stop, double km)
{
	plecare = start;
	destinatie = stop;
	distanta = km;
}

CTraseu::CTraseu(string start, string stop, double km)
{
	plecare = COras(start);
	destinatie = COras(stop);
	distanta = km;
}
double CTraseu::GetDistanta()
{
	return distanta;
}

CTraseu::~CTraseu(void)
{
}
