#include "Autovehicul.h"


CAutovehicul::CAutovehicul(void)
{
}
void CAutovehicul::AdaugareCursa(CCursa cursa)
{
	curse.push_back(cursa);
}

CAutovehicul::CAutovehicul(string inmatriculare, double km, string sofer)
{
	NrInmatriculare = inmatriculare;
	kmParcursi = km;
	soferi.push_back(sofer);
}


CAutovehicul::CAutovehicul(string inmatriculare, double km)
{
	NrInmatriculare = inmatriculare;
	kmParcursi = km;
}

void CAutovehicul::AdaugareSofer(string nume)
{
	soferi.push_back(nume);
}

bool CAutovehicul::CautareSofer(string nume)
{
	list<string>::iterator i;
	for(i = soferi.begin();i!=soferi.end();++i)
	{
		if(*i==nume)
		{
			return true;
		}
	}
	return false;
}

bool CAutovehicul::ComparaNrInmatriculare(string nr)
{
	return (NrInmatriculare == nr);
}

CAutovehicul::~CAutovehicul(void)
{
}
