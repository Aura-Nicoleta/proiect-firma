#include "Firma.h"
#include <list>
using namespace std;
CFirma::CFirma(void)
{
}

	void CFirma::AdaugareAutovehiculeFirma(CAutovehicul masina)
	{
		autovehicule.push_back(masina);
	}
	void CFirma::AdaugareCursaFirma(CCursa cursa)
	{
		curse.push_back(cursa);
	}
CAutovehicul  CFirma::CautareDupaNumarInmatriculare(string NrInmatriculare)
{
	list<CAutovehicul>::iterator i;
	for(i = autovehicule.begin();i!= autovehicule.end();++i)
	{
		if(i->ComparaNrInmatriculare(NrInmatriculare))
		{
			return *i;
		}
	}
}
void CFirma::AlocareCurseAutovehicule()
{
	list<CCursa>::iterator cursa;
	list<CAutovehicul>::iterator autovehicul = autovehicule.begin();
	for(cursa = curse.begin();cursa!=curse.end();cursa++)
	{
		autovehicul->AdaugareCursa(*cursa);
		curse.pop_front();
		if(autovehicul == autovehicule.end())
		{
			autovehicul = autovehicule.begin();
		}
		else
		{
			autovehicul++;
		}
	}
}
void CFirma::AdaugareSoferFirma(string nume)
{
	soferi.push_back(nume);
}
void CFirma::AlocareSoferiAutovehicul()
{
	list<string>::iterator sofer;
	list<CAutovehicul>::iterator autovehicul = autovehicule.begin();
	for(sofer = soferi.begin();sofer!=soferi.end();sofer++)
	{
		autovehicul->AdaugareSofer(*sofer);
		if(autovehicul == autovehicule.end())
		{
			autovehicul = autovehicule.begin();
		}
		else
		{
			autovehicul++;
		}
	}
}

CAutovehicul CFirma::CautareAutovehiculDupaNumeSofer(string nume)
{
	list<CAutovehicul>::iterator i;
	for(i = autovehicule.begin();i!= autovehicule.end();++i)
	{
		if(i->CautareSofer(nume))
		{
			return *i;
		}
	}
}

CFirma::~CFirma(void)
{
}
