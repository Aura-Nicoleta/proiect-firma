#pragma once
#include <iostream>
#include <list>
#include "Autovehicul.h"
#include "Cursa.h"
class CFirma
{
private:
	list<CAutovehicul> autovehicule;
	list<CCursa> curse;
	list<string> soferi;
public:
	void AdaugareAutovehiculeFirma(CAutovehicul masina);
	void AdaugareCursaFirma(CCursa cursa);
	void AlocareCurseAutovehicule();
	void AdaugareSoferFirma(string nume);
	void AlocareSoferiAutovehicul();
	CAutovehicul CautareDupaNumarInmatriculare(string NrInmatriculare);
	CAutovehicul CautareAutovehiculDupaNumeSofer(string nume);
	CFirma(void);
	~CFirma(void);
};

