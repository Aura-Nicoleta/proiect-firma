#pragma once
#include "Oras.h"
class CTraseu
{
private:
	COras plecare;
	COras destinatie;
	double distanta;

public:
	double GetDistanta();
	CTraseu();
	CTraseu(COras start, COras stop, double km);
	CTraseu(string start, string stop, double km);
	~CTraseu(void);
};

