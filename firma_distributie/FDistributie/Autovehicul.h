#pragma once
#include <iostream>
#include <list>
#include <string>
#include "Incarcatura.h"
#include "Cursa.h"
using namespace std;
class CAutovehicul
{
private:
	bool activ;
	string NrInmatriculare;
	double kmParcursi;
	CIncarcatura incarcatura;
	list<CCursa> curse;
	list<string> soferi;
	double consum;

public:
	void AdaugareCursa(CCursa cursa);
	void AdaugareSofer(string nume);
	bool CautareSofer(string nume);
	bool ComparaNrInmatriculare(string nr);
	CAutovehicul(void);
	CAutovehicul(string inmatriculare, double km);
	CAutovehicul(string inmatriculare, double km, string sofer);
	~CAutovehicul(void);
};

