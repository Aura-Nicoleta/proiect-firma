/*38. Gestiunea parcului de masini si a curselor pentru o firma de distributie

Sa se realizeze un proiect pentru gestiunea parcului de masini si a curselor pentru o firma de
distributie. Pentru fiecare autovehicul se cunosc urmatoarele informatii:
 tipul autovehiculului;
 numarul de inmatriculare;
 numarul de kilometri parcursi;
 numele soferului/soferilor;
Pentru fiecare cursa realizata se cunosc informatiile care se refera la masina cu care
aceasta cursa a fost sau va fi realizata, cantitatea de combustibil consumata si traseul
corespunzator cursei.
Operatii cerute:
 alocarea masinilor astfel incat sa se realizeze in mod optim toate cursele;
 realizarea unei revizii pentru fiecare masina dupa parcurgerea unui numar de
kilometri;
 modificare treaseului pentru fiecare cursa astfel incat sa se minimize numarul
de curse;
 cautare autoturism dupa numarul de inmatriculare;
 cautare autoturism dupa numele soferului;
 afisarea autoturismelor cu un numar de kilometri mai mare decat o valoare
specificata;
 sortarea autoturismelor dupa tipul autovehiculului;
 alocarea soferilor astfel incat sa nu avem doua autoturisme cu acelasi sofer;*/
