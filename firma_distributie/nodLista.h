#ifndef NODLISTA_H_INCLUDED
#define NODLISTA_H_INCLUDED

class CNodLista
{
private:
    CNodLista *ElementUrm;
    struct Autovehicul
    {
        string Tip[20];
        string Nr_inmatriculare[9];
        float Km_parcursi;
        string Nume_sofer[3];
    };
    Autovehicul autovh;
public:
    CNodLista();
    CNodLista(Autovehicul&);
    ~CNodLista(void);

    void CitireDate();
    void AdaugareAutovehicul(Autovehicul&);
    void AfisareAutovehicul(Autovehicul);

    Autovehicul GetAutovehicul() {return autovh; } //functie accesor
    string GetTip(){return autovh.Tip; } //functie accesor
    string GetNr_inmatriculare(int i) {return autovh.Nr_inmatriculare[i]; } //functie accesor
    float GetKm_parcursi() {return autovh.Km_parcursi; } //accesor
    string GetNume_sofer(int i) {return autovh.Nume_sofer[i]; } //accesor

    void SetTip(string _Tip) {this->autovh.Tip=_Tip; } //functie modificator
    void SetNr_inmatriculare(int i, string _Nr_inmatriculare) {this->autovh.Nr_inmatriculare[i]=_Nr_inmatriculare; } //functie modificator
    void SetKm_parcursi(float _Km_parcursi) {this->autovh.Km_parcursi=_Km_parcursi; } //modificator
    void SetNume_sofer(int i, string _Nume_sofer) {this->autovh.Nume_sofer[i]=_Nume_sofer; } //functie modificator
};

#endif // NODLISTA_H_INCLUDED
